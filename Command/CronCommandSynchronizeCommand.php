<?php

namespace Nisanov\CronCommandBundle\Command;

use Nisanov\CronCommandBundle\Service\CronCommandManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CronCommandSynchronizeCommand.
 *
 * @package Nisanov\CronCommandBundle\Command
 */
class CronCommandSynchronizeCommand extends ContainerAwareCommand
{
    /**
     * @var CronCommandManager
     */
    private $crontab;

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('cron:command:synchronize');
        $this->setDescription('Synchronize commands with the crontab.');
    }

    /**
     * Initializes the command just after the input has been validated.
     *
     * @SuppressWarnings("PHPMD.UnusedFormalParameter")
     *
     * @param InputInterface $input   An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        // assign an instance reference to the cron command manager service
        $this->crontab = $this->getContainer()->get('nisanov_cron_command.manager');
    }

    /**
     * Executes the current command.
     *
     * @SuppressWarnings("PHPMD.UnusedFormalParameter")
     *
     * @param InputInterface $input   An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     *
     * @return null|int null or 0 if everything went fine, or an error code
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // synchronize the cron commands with the crontab
        $this->crontab->synchronize();

        // log accordingly
        $output->writeln('<info>The commands have been synchronized with the crontab.</info>');

        // return successful state
        return 0;
    }
}
