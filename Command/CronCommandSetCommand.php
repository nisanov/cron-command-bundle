<?php

namespace Nisanov\CronCommandBundle\Command;

use Nisanov\CronCommandBundle\Entity\CronCommand;
use Nisanov\CronCommandBundle\Repository\CronCommandRepository;
use Nisanov\CronCommandBundle\Service\CronCommandManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Class CronCommandSetCommand.
 *
 * @package Nisanov\CronCommandBundle\Command
 */
class CronCommandSetCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var CronCommandRepository
     */
    private $repository;

    /**
     * @var CronCommandManager
     */
    private $crontab;

    /**
     * @var CronCommand
     */
    private $command;

    /**
     * @var QuestionHelper
     */
    private $question;

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('cron:command:set');
        $this->setDescription('Manage commands in the crontab.');
        $this->addOption('remove', null, InputOption::VALUE_NONE, 'Remove the selected cron command.');
    }

    /**
     * Initializes the command just after the input has been validated.
     *
     * @SuppressWarnings("PHPMD.UnusedFormalParameter")
     *
     * @param InputInterface $input   An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        // when no interaction is specified; enforce interaction
        if ($input->getOption('no-interaction')) {
            throw new \InvalidArgumentException('This command requires interaction.');
        }

        // assign a local reference to the container
        $container = $this->getContainer();

        // assign a local reference to doctrine
        $doctrine = $container->get('doctrine');

        // assign an instance reference to the doctrine manipulation objects
        $this->manager = $doctrine->getManager();
        $this->repository = $doctrine->getRepository('NisanovCronCommandBundle:CronCommand');

        // assign an instance reference to the cron command manager service
        $this->crontab = $container->get('nisanov_cron_command.manager');

        // assign an instance reference to the question helper
        $this->question = $this->getHelper('question');
    }

    /**
     * Interacts with the user.
     *
     * @param InputInterface $input   An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        // ask the command name question
        $this->askCommandNameQuestion($input, $output);

        // when a command has been selected
        if ($this->command) {

            // when not removing an existing command
            if (!$input->getOption('remove')) {

                // ask the command parameters question
                $this->askCommandParametersQuestion($input, $output);

                // ask the command schedule question
                $this->askCommandScheduleQuestion($input, $output);

                // ask the command lockable question
                $this->askCommandLockableQuestion($input, $output);

                // ask the command disabled question
                $this->askCommandDisabledQuestion($input, $output);
            }
        }
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface $input   An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     *
     * @return null|int null or 0 if everything went fine, or an error code
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // when a command has been selected
        if ($this->command) {

            if ($input->getOption('remove')) {
                // remove the command from the database
                $this->manager->remove($this->command);
                // log accordingly
                $output->writeln('<error>The selected cron command has been removed.</error>');
            } else {
                // persist the command to the database
                $this->manager->persist($this->command);
                // log accordingly
                $output->writeln('<info>The cron command has the following information applied:</info>');
                $output->writeln($this->getCommandDescription($this->command));
            }

            // flush the database
            $this->manager->flush();

            // synchronize the cron commands with the crontab
            $this->crontab->synchronize();

            // log accordingly
            $output->writeln('<info>The cron command request complete.</info>');
        } else {

            // log accordingly
            $output->writeln('<info>The cron command request cancelled.</info>');
        }

        // return successful state
        return 0;
    }

    /**
     * Asks the command name question.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function askCommandNameQuestion(InputInterface $input, OutputInterface $output)
    {
        // get all the existing command names
        $commands = array_unique(array_column($this->repository->createQueryBuilder('c')->select('c.name')->getQuery()->getArrayResult(), 'name'));

        // handle cron command remove requests
        if ($input->getOption('remove')) {
            // configure the command name question
            $question = new Question('<question>Select an existing console command name to remove:</question> ', '');
            $question->setAutocompleterValues($commands);
            // ask the command name question
            if (($name = $this->question->ask($input, $output, $question))) {
                $this->askCommandSelectionQuestion($name, $input, $output);
            } else {
                throw new \InvalidArgumentException('Command Failed: The command name is required.');
            }
        } // handle cron command set requests
        else {
            // merge in all the available commands
            $commands = array_unique(array_merge($commands, array_keys($this->getApplication()->all())));
            // configure the command name question
            $question = new Question('<question>Select an existing console command name to modify or create a new one:</question> ', '');
            $question->setAutocompleterValues($commands);
            // ask the command name question
            if (($name = $this->question->ask($input, $output, $question))) {
                $this->askCommandSelectionQuestion($name, $input, $output);
            } else {
                throw new \InvalidArgumentException('Command Failed: The command name is required.');
            }
        }
    }

    /**
     * Asks the command selection question.
     *
     * @param string $name
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function askCommandSelectionQuestion(string $name, InputInterface $input, OutputInterface $output)
    {
        // reference all the matching commands
        $commands = $this->repository->findBy(['name' => $name]);

        // when there are commands that have matched or the remove request was not specified
        if (count($commands) || !$input->getOption('remove')) {
            // compile a list of matching cron command descriptions
            $options = array_reduce($commands, function ($items, $command) {
                /** @var CronCommand $command */
                $items[] = $this->getCommandDescription($command);

                return $items;
            }, $items = []);
            // configure the appropriate command selection question
            if ($input->getOption('remove')) {
                $question = new ChoiceQuestion('<question>Select the command to confirm and remove:</question> ', array_merge($options, ['Cancel']));
            } else {
                if (count($commands)) {
                    $question = new ChoiceQuestion('<question>Select the command to confirm and modify or create new:</question> ',
                        array_merge($options, ['Create New', 'Cancel']));
                } else {
                    $question = new ChoiceQuestion('<question>Select to confirm creating a new command or cancel:</question> ', ['Create New', 'Cancel']);
                }
            }
            // ask the command selection question
            if (($command = $this->question->ask($input, $output, $question))) {
                // when the command selection question was not cancelled
                if ('Create New' == $command) {
                    $this->command = new CronCommand();
                    $this->command->setName($name);
                } elseif ('Cancel' != $command) {
                    $this->command = $commands[array_search($command, $options)];
                    $this->command->clearStatus($this->manager);
                }
            }
        } else {
            throw new \UnexpectedValueException('No commands were found matching the selected command name, please restart and try again.');
        }
    }

    /**
     * Asks the command parameters question.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function askCommandParametersQuestion(InputInterface $input, OutputInterface $output)
    {
        // configure the question
        $default = $this->command->getId() ? sprintf('<info>[mod?]</info> <comment>[%s]</comment> ', $this->command->getParameters()) : '<info>[new!]</info> ';
        $question = new Question(sprintf('<question>Enter the console command parameters</question> %s', $default), $this->command->getParameters());
        $question->setAutocompleterValues(['> /dev/null 2>&1']);

        // process the question response
        if (($response = $this->question->ask($input, $output, $question))) {
            $this->command->setParameters($response);
        } else {
            throw new \InvalidArgumentException('Command Failed: Missing the cron command parameter option.');
        }
    }

    /**
     * Asks the command schedule question.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function askCommandScheduleQuestion(InputInterface $input, OutputInterface $output)
    {
        // configure the question
        $default = $this->command->getId() ? sprintf('<info>[mod?]</info> <comment>[%s]</comment> ', $this->command->getSchedule()) : '<info>[new!]</info> ';
        $question = new Question(sprintf('<question>Enter the console command schedule pattern</question> %s', $default), $this->command->getSchedule());
        $question->setAutocompleterValues(['@yearly', '@monthly', '@weekly', '@daily', '@hourly', '*/5 * * * *', '*/5 * * * *', '* * * * *']);

        // process the question response
        if (($response = $this->question->ask($input, $output, $question))) {
            $this->command->setSchedule($response);
        } else {
            throw new \InvalidArgumentException('Command Failed: Missing the cron command schedule option.');
        }
    }

    /**
     * Asks the command lockable question.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function askCommandLockableQuestion(InputInterface $input, OutputInterface $output)
    {
        // configure the question
        $default = $this->command->getId() ? sprintf('<info>[mod?]</info> <comment>[%s]</comment> ', $this->command->isLockable() ? 'Yes' : 'No') : '<info>[new!]</info> ';
        $question = new Question(sprintf('<question>Is this command lockable [No/Yes]?</question> %s', $default), $this->command->isLockable() ? 'Yes' : 'No');
        $question->setAutocompleterValues(['No', 'Yes']);

        // process the question response
        if (($response = $this->question->ask($input, $output, $question))) {
            $this->command->setLockable('Yes' == $response);
        }
    }

    /**
     * Asks the command disabled question.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function askCommandDisabledQuestion(InputInterface $input, OutputInterface $output)
    {
        // configure the question
        $default = $this->command->getId() ? sprintf('<info>[mod?]</info> <comment>[%s]</comment> ', $this->command->isDisabled() ? 'Yes' : 'No') : '<info>[new!]</info> ';
        $question = new Question(sprintf('<question>Is this command disabled [No/Yes]?</question> %s', $default), $this->command->isDisabled() ? 'Yes' : 'No');
        $question->setAutocompleterValues(['No', 'Yes']);

        // process the question response
        if (($response = $this->question->ask($input, $output, $question))) {
            $this->command->setDisabled('Yes' == $response);
        }
    }

    /**
     * Gets the cron command description string.
     *
     * @param CronCommand $command
     *
     * @return string
     */
    private function getCommandDescription(CronCommand $command): string
    {
        return sprintf('<comment>%s %s%s</comment>', $this->crontab->toCrontabString($command),
            $command->isLockable() ? '<info>[Lockable]</info>' : '<info>[Asynchronous]</info>', $command->isDisabled() ? '<info>[Disabled]</info>' : '<info>[Active]</info>');
    }
}
