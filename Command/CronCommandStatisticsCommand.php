<?php

namespace Nisanov\CronCommandBundle\Command;

use Nisanov\CronCommandBundle\Service\CronCommandManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CronCommandStatisticsCommand.
 *
 * @package Nisanov\CronCommandBundle\Command
 */
class CronCommandStatisticsCommand extends ContainerAwareCommand
{
    /**
     * @var CronCommandManager
     */
    private $crontab;

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('cron:command:statistics');
        $this->setDescription('Display statistical information on the managed crontab commands.');
    }

    /**
     * Initializes the command just after the input has been validated.
     *
     * @SuppressWarnings("PHPMD.UnusedFormalParameter")
     *
     * @param InputInterface $input   An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        // assign a local reference to the container
        $container = $this->getContainer();

        // assign an instance reference to the cron command manager service
        $this->crontab = $container->get('nisanov_cron_command.manager');
    }

    /**
     * Executes the current command.
     *
     * @SuppressWarnings("PHPMD.UnusedFormalParameter")
     *
     * @param InputInterface $input   An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     *
     * @return null|int null or 0 if everything went fine, or an error code
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $table = new Table($output);

        $statistics = $this->crontab->statistics();

        // sort by date first
        usort($statistics, function($pos1, $pos2) {
            return $pos1['nextRunAt'] <=> $pos2['nextRunAt'];
        });
        // sort by overdue next
        usort($statistics, function($pos1, $pos2) {
            return $pos2['isOverdue'] <=> $pos1['isOverdue'];
        });

        $table->setHeaders(['Name', 'Schedule', 'Created At', 'Last Success Ran At', 'Next Run At', 'Is Overdue', 'Exit Code', 'Exit Message'])->setRows($statistics);

        $table->setStyle('borderless');
        $table->render();

        // return successful state
        return 0;
    }
}
