<?php

namespace Nisanov\CronCommandBundle\Service;

use Cron\CronExpression;
use Doctrine\ORM\EntityManager;
use Nisanov\CronCommandBundle\Entity\CronCommand;
use Nisanov\CronCommandBundle\Entity\CronCommandStatus;
use Nisanov\CronCommandBundle\Repository\CronCommandStatusRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Process\Process;

/**
 * Class CronCommandManager.
 *
 * @package Nisanov\CronCommandBundle\Service
 */
class CronCommandManager
{
    /**
     * The doctrine entity manager.
     *
     * @var EntityManager
     */
    private $manager;

    /**
     * The cron command lines of the crontab
     *
     * @var array
     */
    private $crontab;

    /**
     * The applicable crontab php and kernel root paths.
     *
     * @var array
     */
    private $paths;

    /**
     * CronCommandManager constructor.
     *
     * @param RegistryInterface $registry
     * @param string $rootDir
     */
    public function __construct(RegistryInterface $registry, string $rootDir)
    {
        // assign the doctrine entity manager reference
        $this->manager = $registry->getManager();

        // derive the php path
        $this->paths['php'] = new Process('which php');
        $this->paths['php']->run();
        $this->paths['php'] = trim($this->paths['php']->getOutput());

        // derive the symfony console path
        $this->paths['console'] = dirname($rootDir).'/bin/console';

        // run the process to list the crontab
        $process = new Process('crontab -l');
        $process->run();

        // create an array of crontab lines
        $this->crontab = explode(PHP_EOL, trim($process->getOutput()));
    }

    /**
     * Synchronizes the cron commands with the cron table.
     */
    public function synchronize()
    {
        // reference all the commands
        $commands = $this->manager->getRepository('NisanovCronCommandBundle:CronCommand')->findBy(['disabled' => false]);
        // extract the crontab string
        $commands = array_reduce($commands, function ($items, $command) {
            /** @var CronCommand $command */
            $items[] = $this->toCrontabString($command).' # code-corn-cron-command';

            return $items;
        }, $items = []);

        $exportable = [];
        // step through all the existing crontab lines
        foreach ($this->crontab as $line) {
            // filter out all cron commands
            if (false === strpos($line, '# code-corn-cron-command')) {
                $exportable[] = $line;
            }
        }
        // append the active cron command lines
        $exportable = array_merge($exportable, $commands);

        $file = tempnam(sys_get_temp_dir(), 'cron');

        file_put_contents($file, implode(PHP_EOL, $exportable).PHP_EOL);

        $process = new Process('crontab '.$file);
        $process->run();
    }

    /**
     * Generates statistics.
     *
     * @return array
     */
    public function statistics(): array
    {
        /** @var CronCommandStatusRepository $statuses */
        // assign a reference to a local instance of the cron command status repository
        $statuses = $this->manager->getRepository('NisanovCronCommandBundle:CronCommandStatus');
        // get all the active cron commands
        $commands = $this->manager->getRepository('NisanovCronCommandBundle:CronCommand')->findBy(['disabled' => false]);
        // compile the statistics on the active cron commands
        $commands = array_reduce($commands, function ($items, $command) use ($statuses) {
            // get the last successful status and the last status of any kind
            $status['success'] = $statuses->findByScopeLastSuccessfulByCommand($command);
            $status['last'] = $statuses->findByScopeLastByCommand($command);
            /** @var CronCommand $command */
            $items[] = [
                'name' => $command->getName(),
                'schedule' => $command->getSchedule(),
                'createdAt' => $command->getCreatedAt()->format('Y-m-d H:i:s'),
                'lastSuccessRanAt' => $status['success'] && ($lastRanAt = $status['success']->getRanAt()) ? $lastRanAt->format('Y-m-d H:i:s') : '',
                'nextRunAt' => CronExpression::factory($command->getSchedule())->getNextRunDate()->format('Y-m-d H:i:s'),
                'isOverdue' => (int)$this->isOverdue($command, $status['success'] ?: false),
                'exitCode' => $status['last'] ? $status['last']->getExitCode() : '',
                'exitMessage' => $status['last'] ? $status['last']->getExitMessage() : '',
            ];

            return $items;
        }, $items = []);

        // return the command statistics
        return $commands;
    }

    /**
     * Converts a cron command to a crontab line.
     *
     * @param CronCommand $command
     *
     * @return string
     */
    public function toCrontabString(CronCommand $command): string
    {
        // return the crontab string
        return sprintf('%s %s %s %s %s', $command->getSchedule(), $this->paths['php'], $this->paths['console'], $command->getName(), $command->getParameters());
    }

    /**
     * Determines whether this command is overdue.
     *
     * @SuppressWarnings("PHPMD.StaticAccess")
     *
     * @param CronCommand $command
     * @param CronCommandStatus|bool $status
     *
     * @return bool
     */
    public function isOverdue(CronCommand $command, $status = null): bool
    {
        /** @var CronCommandStatus $status */

        // when there is no explicit specification on no status
        if (false !== $status) {
            // use the specified status or find the latest scoped status
            if ($status = ($status ?: $this->manager->getRepository('NisanovCronCommandBundle:CronCommandStatus')->findByScopeLastSuccessfulByCommand($command))) {
                $lastRunDate = $status->getRanAt();
                $lastRunDate->setTime($lastRunDate->format('H'), $lastRunDate->format('i'));
            }
        }

        $cron = CronExpression::factory($command->getSchedule());
        $nextRunDate = $cron->getNextRunDate($lastRunDate ?? $command->getCreatedAt());
        $nextRunDate->setTime($nextRunDate->format('H'), $nextRunDate->format('i'), CronCommand::TRANSITION_DELAY);

        return new \DateTime() > $nextRunDate;
    }

    /**
     * Gets the derived crontab applicable paths.
     *
     * @return array
     */
    public function getPaths(): array
    {
        return $this->paths;
    }
}