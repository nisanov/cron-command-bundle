<?php

namespace Nisanov\CronCommandBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Nisanov\CronCommandBundle\Entity\CronCommand;
use Nisanov\CronCommandBundle\Entity\CronCommandStatus;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Event\ConsoleExceptionEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;

/**
 * Class CommandListener.
 *
 * @package Nisanov\CronCommandBundle\EventListener
 */
class CommandListener
{
    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * The unique process token.
     *
     * @var string
     */
    private $token;

    /**
     * CommandListener constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        $this->manager = $registry->getManager();
    }

    /**
     * Handles the console command event.
     *
     * @param ConsoleCommandEvent $event
     */
    public function consoleCommand(ConsoleCommandEvent $event)
    {
        // assign a reference to the event command
        $command = $event->getCommand();

        // when this command is whitelisted
        if ($this->isCommandWhitelisted($command)) {

            // when this command is being tracked
            if ($command = $this->getCommand($command)) {

                $output = $event->getOutput();

                if ($command->isDisabled()) {
                    $event->disableCommand();
                    $output->writeln(sprintf('<error>The command [%s] is disabled.</error>', $command->getName()));
                } elseif ($command->isLockable()) {
                    if ($command->isLocked()) {
                        $event->disableCommand();
                        $output->writeln(sprintf('<error>The command [%s] is locked by another process.</error>', $command->getName()));
                    } else {
                        $command->setLocked(true);
                        $this->manager->persist($command);
                    }
                }

                $status = new CronCommandStatus();
                $status->setCronCommand($command);
                $status->setToken($this->token = md5(uniqid()));

                $this->manager->persist($status);
                $this->manager->flush();
            }
        }
    }

    /**
     * Handles the console command exception event.
     *
     * @param ConsoleExceptionEvent $event
     */
    public function consoleException(ConsoleExceptionEvent $event)
    {
        // assign a reference to the event command
        $command = $event->getCommand();

        // when this command is whitelisted
        if ($this->isCommandWhitelisted($command)) {

            // when this command is being tracked
            if ($command = $this->getCommand($command)) {

                $exception = $event->getException();

                if ($command->isLockable() && $command->isLocked()) {
                    $command->setLocked(false);
                    $this->manager->persist($command);
                }

                if (!($status = $this->getStatus($command, $this->token))) {
                    $status = new CronCommandStatus();
                    $status->setCronCommand($command);
                    $status->setToken($this->token = md5(uniqid()));
                }

                $status->setExitCode($event->getExitCode());
                $status->setExitMessage($exception->getMessage());

                $this->manager->persist($status);
                $this->manager->flush();
            }
        }
    }

    /**
     * Handles the console command termination event.
     *
     * @param ConsoleTerminateEvent $event
     */
    public function consoleTerminated(ConsoleTerminateEvent $event)
    {
        // assign a reference to the event command
        $command = $event->getCommand();

        // when this command is whitelisted
        if ($this->isCommandWhitelisted($command)) {

            // when this command is being tracked
            if ($command = $this->getCommand($command)) {

                if (!($status = $this->getStatus($command, $this->token))) {
                    $status = new CronCommandStatus();
                    $status->setCronCommand($command);
                    $status->setToken($this->token = md5(uniqid()));
                }

                if ($command->isLocked() && CronCommandStatus::RETURN_CODE_DISABLED == $event->getExitCode()) {
                    $status->setExitCode(CronCommandStatus::RETURN_CODE_LOCKED);
                } else {
                    $status->setExitCode($event->getExitCode());
                }

                if ($command->isLockable() && $command->isLocked()) {
                    $command->setLocked(false);
                    $this->manager->persist($command);
                }

                $this->manager->persist($status);
                $this->manager->flush();
            }
        }
    }

    /**
     * Determine whether the command is whitelisted.
     *
     * @param Command $command
     *
     * @return bool
     */
    private function isCommandWhitelisted(Command $command): bool
    {
        // return false for blacklisted commands.
        if (preg_match('/^(doctrine|server|cache|assets):/', $command->getName())) {
            return false;
        }

        return true;
    }

    /**
     * Gets the associated cron command, as applicable.
     *
     * @param Command $command
     *
     * @return CronCommand|null
     */
    private function getCommand(Command $command): ?CronCommand
    {
        return $this->manager->getRepository('NisanovCronCommandBundle:CronCommand')->findOneBy(['name' => $command->getName()]);
    }

    /**
     * Gets the associated cron command status, as applicable.
     *
     * @param CronCommand $command
     * @param string $token
     *
     * @return CronCommandStatus|null
     */
    private function getStatus(CronCommand $command, string $token = null): ?CronCommandStatus
    {
        return $this->manager->getRepository('NisanovCronCommandBundle:CronCommandStatus')->findOneBy(['cronCommand' => $command, 'token' => $token]);
    }
}