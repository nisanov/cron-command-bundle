<?php

namespace Nisanov\CronCommandBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class CronCommandStatus
 *
 * @ORM\Table(name="cron_command_status", uniqueConstraints={@ORM\UniqueConstraint(name="unique_token", columns={"token"})})
 * @ORM\Entity(repositoryClass="Nisanov\CronCommandBundle\Repository\CronCommandStatusRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @package Nisanov\CronCommandBundle\Entity
 */
class CronCommandStatus
{
    /**
     * The cron command status return codes.
     *
     * @var int
     */
    const RETURN_CODE_SUCCESSFUL = 0;
    const RETURN_CODE_MISSING_OPTIONS = 111;
    const RETURN_CODE_LOCKED = 112;
    const RETURN_CODE_DISABLED = 113;

    /**
     * The unique record identifier.
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * The cron command.
     *
     * @var CronCommand
     *
     * @ORM\ManyToOne(targetEntity="Nisanov\CronCommandBundle\Entity\CronCommand", inversedBy="status")
     * @ORM\JoinColumn(name="cron_command_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $cronCommand;

    /**
     * The unique process token.
     *
     * @var string
     *
     * @ORM\Column(name="token", type="string")
     */
    private $token;

    /**
     * When the cron command was executed.
     *
     * @var \DateTime
     *
     * @ORM\Column(name="ran_at", type="datetime")
     */
    private $ranAt;

    /**
     * The cron command execution exit code.
     *
     * @var int
     *
     * @ORM\Column(name="exit_code", type="integer", nullable=true)
     */
    private $exitCode;

    /**
     * The cron command execution exit message, as applicable.
     *
     * @var string
     *
     * @ORM\Column(name="exit_message", type="string", length=255, nullable=true)
     */
    private $exitMessage;

    /**
     * Gets the unique record identifier.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Gets the associated command.
     *
     * @return CronCommand
     */
    public function getCronCommand(): CronCommand
    {
        return $this->cronCommand;
    }

    /**
     * Sets the associated command.
     *
     * @param CronCommand $cronCommand
     *
     * @return CronCommandStatus
     */
    public function setCronCommand(CronCommand $cronCommand)
    {
        $this->cronCommand = $cronCommand;

        return $this;
    }

    /**
     * Gets the unique process token.
     *
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * Sets the unique process token.
     *
     * @param string $token
     *
     * @return CronCommandStatus
     */
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Gets when the cron command was run.
     *
     * @return \DateTime
     */
    public function getRanAt(): \DateTime
    {
        return $this->ranAt;
    }

    /**
     * Sets when the cron command was run.
     *
     * @param \DateTime $ranAt
     *
     * @return CronCommandStatus
     */
    public function setRanAt(\DateTime $ranAt): self
    {
        $this->ranAt = $ranAt;

        return $this;
    }

    /**
     * Gets the cron command exit code.
     *
     * @return int|null
     */
    public function getExitCode(): ?int
    {
        return $this->exitCode;
    }

    /**
     * Sets the cron command exit code.
     *
     * @param integer $exitCode
     *
     * @return CronCommandStatus
     */
    public function setExitCode(int $exitCode): self
    {
        $this->exitCode = $exitCode;

        return $this;
    }

    /**
     * Gets the cron command exit message.
     *
     * @return string
     */
    public function getExitMessage(): ?string
    {
        return $this->exitMessage;
    }

    /**
     * Sets the cron command exit message.
     *
     * @param string $exitMessage
     *
     * @return CronCommandStatus
     */
    public function setExitMessage(string $exitMessage): self
    {
        $this->exitMessage = $exitMessage;

        return $this;
    }

    /**
     * Performed before the database insert operations on entity data.
     *
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->ranAt = new \DateTime();
    }

    /**
     * Performed before the database update operations on entity data.
     *
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->ranAt = new \DateTime();
    }
}

