<?php

namespace Nisanov\CronCommandBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Class CronCommand.
 *
 * @ORM\Table(name="cron_command", indexes={@ORM\Index(name="search_idx", columns={"name"})})
 * @ORM\Entity(repositoryClass="Nisanov\CronCommandBundle\Repository\CronCommandRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @package Nisanov\CronCommandBundle\Entity
 */
class CronCommand
{
    /**
     * The number of seconds to allow for the process transition delay.
     *
     * @var int
     */
    const TRANSITION_DELAY = 5;

    /**
     * The unique record identifier.
     *
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * The cron command status.
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Nisanov\CronCommandBundle\Entity\CronCommandStatus", mappedBy="cronCommand", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
     * @ORM\OrderBy({"ranAt" = "DESC"})
     */
    private $status;

    /**
     * The schedule of the cron command.
     *
     * @var string
     *
     * @ORM\Column(name="schedule", type="string", length=50)
     */
    private $schedule;

    /**
     * The name of the cron command.
     *
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * The parameters of the cron command.
     *
     * @var string
     *
     * @ORM\Column(name="parameters", type="string", length=255)
     */
    private $parameters;

    /**
     * Whether the command is disabled.
     *
     * @var bool
     *
     * @ORM\Column(name="disabled", type="boolean")
     */
    private $disabled = false;

    /**
     * Whether the command is locks during processing and only allows one instance running.
     *
     * @var bool
     *
     * @ORM\Column(name="lockable", type="boolean")
     */
    private $lockable = false;

    /**
     * Whether the command is locked during processing.
     *
     * @var bool
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    private $locked = false;

    /**
     * The created at date time.
     *
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * The update at date time.
     *
     * @ORM\Column(name="updated_at", type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * CronCommand constructor.
     */
    public function __construct()
    {
        $this->status = new ArrayCollection();
    }

    /**
     * Gets the unique record identifier.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Gets the cron command status.
     *
     * @return ArrayCollection
     */
    public function getStatus(): ArrayCollection
    {
        return $this->status instanceof PersistentCollection ? new ArrayCollection($this->status->getValues()) : $this->status;
    }

    /**
     * Adds a cron command status.
     *
     * @param CronCommandStatus $status
     *
     * @return CronCommand
     */
    public function addStatus(CronCommandStatus $status): self
    {
        $this->status[] = $status;

        return $this;
    }

    /**
     * Removes a cron command status.
     *
     * @param CronCommandStatus $status
     *
     * @return CronCommand
     */
    public function removeStatus(CronCommandStatus $status): self
    {
        $this->status->removeElement($status);

        return $this;
    }

    /**
     * Clears all cron command status.
     *
     * @param EntityManagerInterface $manager
     *
     * @return CronCommand
     */
    public function clearStatus(EntityManagerInterface $manager): self
    {
        foreach ($this->status as $status) {
            $manager->remove($status);
        }

        $this->status = new ArrayCollection();

        return $this;
    }

    /**
     * Gets the command schedule.
     *
     * @return string
     */
    public function getSchedule(): ?string
    {
        return $this->schedule;
    }

    /**
     * Sets the command schedule.
     *
     * @param string $schedule
     *
     * @return CronCommand
     */
    public function setSchedule(string $schedule): self
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Gets the command name.
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Sets the command name.
     *
     * @param string $name
     *
     * @return CronCommand
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the command parameters.
     *
     * @return string
     */
    public function getParameters(): ?string
    {
        return $this->parameters;
    }

    /**
     * Sets the command parameters.
     *
     * @param string $parameters
     *
     * @return CronCommand
     */
    public function setParameters(string $parameters): self
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * Gets the disabled state of the command.
     *
     * @return bool
     */
    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    /**
     * Sets the disabled state of the command.
     *
     * @param bool $disabled
     *
     * @return CronCommand
     */
    public function setDisabled(bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Gets the lockable capabilities of the command.
     *
     * @return bool
     */
    public function isLockable(): bool
    {
        return $this->lockable;
    }

    /**
     * Sets the lockable capabilities of the command.
     *
     * @param bool $lockable
     *
     * @return CronCommand
     */
    public function setLockable(bool $lockable): self
    {
        $this->lockable = $lockable;

        return $this;
    }

    /**
     * Gets the locked state of the command.
     *
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->locked;
    }

    /**
     * Sets the locked state of the command.
     *
     * @param bool $locked
     *
     * @return CronCommand
     */
    public function setLocked(bool $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Gets the created at date time.
     *
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * Sets the created at date time.
     *
     * @param \DateTime $createdAt
     *
     * @return CronCommand
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Gets the updated at date time.
     *
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * Sets the updated at date time.
     *
     * @param \DateTime $updatedAt
     *
     * @return CronCommand
     */
    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Performed before the database insert operations on entity data.
     *
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Performed before the database update operations on entity data.
     *
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}