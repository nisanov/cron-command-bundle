<?php

use Composer\Autoload\ClassLoader;
use Doctrine\Common\Annotations\AnnotationRegistry;

/** @var ClassLoader $loader */

if (file_exists(__DIR__.'/../../../vendor/autoload.php')) {
    /** @noinspection PhpIncludeInspection */
    $loader = require __DIR__.'/../../../vendor/autoload.php';
} elseif (file_exists(__DIR__.'/../../../../../autoload.php')) {
    /** @noinspection PhpIncludeInspection */
    $loader = require __DIR__.'/../../../../../autoload.php';
} else {
    throw new \Exception('Missing vendor autoload.php file.');
}

AnnotationRegistry::registerLoader([$loader, 'loadClass']);

return $loader;
