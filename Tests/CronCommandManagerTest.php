<?php

namespace Nisanov\CronCommandBundle\Tests;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Nisanov\CronCommandBundle\Entity\CronCommand;
use Nisanov\CronCommandBundle\Entity\CronCommandStatus;
use Nisanov\CronCommandBundle\Repository\CronCommandRepository;
use Nisanov\CronCommandBundle\Repository\CronCommandStatusRepository;
use Nisanov\CronCommandBundle\Service\CronCommandManager;
use PHPUnit\Framework\TestCase;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Process\Process;

/**
 * Class CronCommandManagerTest.
 *
 * @package Nisanov\CronCommandBundle\Tests
 */
class CronCommandManagerTest extends TestCase
{
    /**
     * Tests that the toCrontabString method performs as expected.
     */
    public function testToCrontabString()
    {
        $manager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();

        $registry = $this->getMockBuilder(Registry::class)->disableOriginalConstructor()->getMock();
        $registry->expects($this->once())->method('getManager')->willReturn($manager);

        /** @var RegistryInterface $registry */
        $cronCommandManager = new CronCommandManager($registry, '/var/www/application/app');

        $command = new CronCommand();
        $command->setName('test:command');
        $command->setSchedule('*/5 * * * *');
        $command->setParameters('> /dev/null 2>&1');

        /** @var CronCommandManager $cronCommandManager */
        $paths = $cronCommandManager->getPaths();
        $expression = $cronCommandManager->toCrontabString($command);

        $this->assertEquals(sprintf('*/5 * * * * %s /var/www/application/bin/console test:command > /dev/null 2>&1', $paths['php']), $expression);
    }

    /**
     * Tests that the isOverdue method performs as expected.
     */
    public function testIsOverdue()
    {
        $now = new \DateTime();

        $status = new CronCommandStatus();
        $status->setRanAt($now);

        $command = new CronCommand();
        $command->setSchedule('*/5 * * * *');

        $repository = $this->getMockBuilder(CronCommandStatusRepository::class)->disableOriginalConstructor()->getMock();
        $repository->expects($this->exactly(2))->method('findByScopeLastSuccessfulByCommand')->with($command)->willReturn($status);

        $manager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $manager->expects($this->exactly(2))->method('getRepository')->with('NisanovCronCommandBundle:CronCommandStatus')->willReturn($repository);

        $registry = $this->getMockBuilder(Registry::class)->disableOriginalConstructor()->getMock();
        $registry->expects($this->once())->method('getManager')->willReturn($manager);

        /** @var RegistryInterface $registry */
        $cronCommandManager = new CronCommandManager($registry, '/var/www/application/app');

        $this->assertFalse($cronCommandManager->isOverdue($command));

        $status->setRanAt($now->modify('-6 minutes'));

        $this->assertTrue($cronCommandManager->isOverdue($command));
    }

    /**
     * Tests that the statistics method performs as expected.
     */
    public function testStatistics()
    {
        // @formatter:off
        $now = new \DateTime();

        /** @var CronCommandStatus[] $statuses */
        $statuses = [new CronCommandStatus(), new CronCommandStatus()];
        $statuses[0]->setRanAt($now);
        $statuses[1]->setRanAt((clone $now)->modify('-6 minutes'));

        /** @var CronCommand[] $commands */
        $commands = [new CronCommand(), new CronCommand(), new CronCommand(), new CronCommand()];
        $commands[0]->setName('cron:command:no:status:no:overdue');
        $commands[0]->setSchedule('*/5 * * * *');
        $commands[0]->setCreatedAt($now);
        $commands[1]->setName('cron:command:no:status:is:overdue');
        $commands[1]->setSchedule('*/5 * * * *');
        $commands[1]->setCreatedAt((clone $now)->modify('-6 minutes'));
        $commands[2]->setName('cron:command:has:status:no:overdue');
        $commands[2]->setSchedule('*/5 * * * *');
        $commands[2]->setCreatedAt($now);
        $commands[3]->setName('cron:command:has:status:is:overdue');
        $commands[3]->setSchedule('*/5 * * * *');
        $commands[3]->setCreatedAt($now);

        $repository['command'] = $this->getMockBuilder(CronCommandRepository::class)->disableOriginalConstructor()->getMock();
        $repository['command']->expects($this->once())->method('findBy')->with(['disabled' => false])->willReturn($commands);

        $repository['status'] = $this->getMockBuilder(CronCommandStatusRepository::class)->disableOriginalConstructor()->getMock();
        $repository['status']
            ->expects($this->exactly(4))
            ->method('findByScopeLastSuccessfulByCommand')
            ->withConsecutive([$commands[0]], [$commands[1]])
            ->willReturnOnConsecutiveCalls(null, null, $statuses[0], $statuses[1]);

        $manager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $manager
            ->expects($this->exactly(2))
            ->method('getRepository')
            ->withConsecutive(['NisanovCronCommandBundle:CronCommandStatus'], ['NisanovCronCommandBundle:CronCommand'])
            ->willReturnOnConsecutiveCalls($repository['status'], $repository['command'], $repository['status'], $repository['status']);

        $registry = $this->getMockBuilder(Registry::class)->disableOriginalConstructor()->getMock();
        $registry->expects($this->once())->method('getManager')->willReturn($manager);

        /** @var RegistryInterface $registry */
        $cronCommandManager = new CronCommandManager($registry, '/var/www/application/app');

        $statistics = $cronCommandManager->statistics();

        $this->assertCount(4, $statistics);
        $this->assertEquals([0, 1, 0, 1], array_column($statistics, 'isOverdue'));
        $this->assertArrayHasKey('name', $statistics[0]);
        $this->assertArrayHasKey('schedule', $statistics[0]);
        $this->assertArrayHasKey('createdAt', $statistics[0]);
        $this->assertArrayHasKey('lastSuccessRanAt', $statistics[0]);
        $this->assertArrayHasKey('nextRunAt', $statistics[0]);
        $this->assertArrayHasKey('exitCode', $statistics[0]);
        $this->assertArrayHasKey('exitMessage', $statistics[0]);
        // @formatter:on
    }

    /**
     * Tests that the synchronize method performs as expected.
     */
    public function testSynchronize()
    {
        // archive the crontab
        $process = new Process('crontab -l');
        $process->run();
        $archive = trim($process->getOutput());

        /** @var CronCommand[] $commands */
        $commands = [new CronCommand(), new CronCommand(), new CronCommand(), new CronCommand()];
        $commands[0]->setName('cron:command:daily');
        $commands[0]->setSchedule('@daily');
        $commands[1]->setName('cron:command:weekly');
        $commands[1]->setSchedule('@weekly');
        $commands[2]->setName('cron:command:monthly');
        $commands[2]->setSchedule('@monthly');
        $commands[3]->setName('cron:command:yearly');
        $commands[3]->setSchedule('@yearly');

        $repository = $this->getMockBuilder(CronCommandRepository::class)->disableOriginalConstructor()->getMock();
        $repository->expects($this->once())->method('findBy')->with(['disabled' => false])->willReturn($commands);

        $manager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $manager->expects($this->once())->method('getRepository')->with('NisanovCronCommandBundle:CronCommand')->willReturn($repository);

        $registry = $this->getMockBuilder(Registry::class)->disableOriginalConstructor()->getMock();
        $registry->expects($this->once())->method('getManager')->willReturn($manager);

        /** @var RegistryInterface $registry */
        $cronCommandManager = new CronCommandManager($registry, '/var/www/application/app');
        $cronCommandManager->synchronize();

        $paths = $cronCommandManager->getPaths();

        $process = new Process('crontab -l');
        $process->run();
        $crontab = trim($process->getOutput());

        $this->assertContains(sprintf('@daily %s /var/www/application/bin/console cron:command:daily  # code-corn-cron-command', $paths['php']), $crontab);
        $this->assertContains(sprintf('@weekly %s /var/www/application/bin/console cron:command:weekly  # code-corn-cron-command', $paths['php']), $crontab);
        $this->assertContains(sprintf('@monthly %s /var/www/application/bin/console cron:command:monthly  # code-corn-cron-command', $paths['php']), $crontab);
        $this->assertContains(sprintf('@yearly %s /var/www/application/bin/console cron:command:yearly  # code-corn-cron-command', $paths['php']), $crontab);

        // revert the crontab
        $file = tempnam(sys_get_temp_dir(), 'archive-crontab');
        file_put_contents($file, $archive.PHP_EOL);
        $process = new Process('crontab '.$file);
        $process->run();
    }
}
