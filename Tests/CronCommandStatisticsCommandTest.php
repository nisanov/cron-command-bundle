<?php

namespace Nisanov\CronCommandBundle\Tests;

use Nisanov\CronCommandBundle\Command\CronCommandStatisticsCommand;
use Nisanov\CronCommandBundle\Entity\CronCommand;
use Nisanov\CronCommandBundle\Entity\CronCommandStatus;
use Nisanov\CronCommandBundle\Tests\TestCase\CommandTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class CronCommandStatisticsCommandTest.
 *
 * @package Nisanov\CronCommandBundle\Tests
 */
class CronCommandStatisticsCommandTest extends CommandTestCase
{
    /**
     * Tests that the cron command statistics command performs as expected.
     */
    public function testCronCommandSetCommand()
    {
        $command = new CronCommand();
        $command->setName('cron:command:daily');
        $command->setParameters('> /dev/null 2>&1');
        $command->setSchedule('@daily');
        $command->setLockable(true);
        $command->setDisabled(false);

        $status = new CronCommandStatus();
        $status->setCronCommand($command);
        $status->setToken(uniqid());
        $status->setRanAt((new \DateTime())->setTime(0, 0, 0));
        $status->setExitCode(0);

        self::$manager->persist($command);
        self::$manager->persist($status);
        self::$manager->flush();

        $command = new CronCommand();
        $command->setName('cron:command:hourly');
        $command->setParameters('> /dev/null 2>&1');
        $command->setSchedule('@hourly');
        $command->setLockable(true);
        $command->setDisabled(false);

        self::$manager->persist($command);
        self::$manager->flush();

        $command = new CronCommand();
        $command->setName('cron:command:failed');
        $command->setParameters('> /dev/null 2>&1');
        $command->setSchedule('@weekly');
        $command->setLockable(true);
        $command->setDisabled(false);

        $status = new CronCommandStatus();
        $status->setCronCommand($command);
        $status->setToken(uniqid());
        $status->setRanAt((new \DateTime())->setTime(0, 0, 0));
        $status->setExitCode(-1);
        $status->setExitMessage('Call to member function getInformation on null');

        self::$manager->persist($command);
        self::$manager->persist($status);
        self::$manager->flush();

        $application = new Application(self::$kernel);
        $application->add(new CronCommandStatisticsCommand());

        $command = $application->find('cron:command:statistics');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName()]);

        $output = $tester->getDisplay();

        $this->assertContains('cron:command:daily', $output);
        $this->assertContains('@daily', $output);

        $this->assertContains('cron:command:hourly', $output);
        $this->assertContains('@hourly', $output);

        $this->assertContains('Call to member function getInformation on null', $output);
    }
}
