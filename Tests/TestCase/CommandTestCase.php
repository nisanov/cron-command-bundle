<?php

namespace Nisanov\CronCommandBundle\Tests\TestCase;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class CommandTestCase.
 *
 * @package Nisanov\CronCommandBundle\Tests\TestCase
 */
class CommandTestCase extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    public static $manager;

    /**
     * Prime the test case by create the database schema.
     */
    public static function prime()
    {
        // Make sure we are in the test environment
        if ('test' !== self::$kernel->getEnvironment()) {
            throw new \LogicException('Primer must be executed in the test environment');
        }

        // Get the entity manager from the service container
        self::$manager = self::$kernel->getContainer()->get('doctrine')->getManager();

        // Run the schema update tool using our entity metadata
        $metadatas = self::$manager->getMetadataFactory()->getAllMetadata();
        $schemaTool = new SchemaTool(self::$manager);
        $schemaTool->createSchema($metadatas);
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        // boot the kernel
        self::bootKernel();

        // prime the test case
        self::prime();
    }
}
