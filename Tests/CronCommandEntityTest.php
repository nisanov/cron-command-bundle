<?php

namespace Nisanov\CronCommandBundle\Tests;

use Doctrine\ORM\EntityManager;
use Nisanov\CronCommandBundle\Entity\CronCommand;
use Nisanov\CronCommandBundle\Entity\CronCommandStatus;
use Nisanov\CronCommandBundle\Tests\TestCase\CommandTestCase;

/**
 * Class CronCommandEntityTest.
 *
 * @package Nisanov\CronCommandBundle\Tests
 */
class CronCommandEntityTest extends CommandTestCase
{
    /**
     * Tests that the cron command entities perform as expected.
     */
    public function testCronCommandSetCommand()
    {
        $now = new \DateTime();

        $command = new CronCommand();
        $command->setName('cron:command:daily');
        $command->setParameters('> /dev/null 2>&1');
        $command->setSchedule('@daily');
        $command->setLockable(true);
        $command->setDisabled(false);
        $command->setLocked(true);
        $command->setCreatedAt($now);
        $command->setUpdatedAt($now);

        $this->assertEquals('cron:command:daily', $command->getName());
        $this->assertEquals('> /dev/null 2>&1', $command->getParameters());
        $this->assertEquals('@daily', $command->getSchedule());
        $this->assertTrue($command->isLockable());
        $this->assertFalse($command->isDisabled());
        $this->assertTrue($command->isLocked());
        $this->assertEquals($now, $command->getCreatedAt());
        $this->assertEquals($now, $command->getUpdatedAt());

        /** @var CronCommandStatus[] $status */

        $status = [new CronCommandStatus(), new CronCommandStatus()];

        $status[0]->setCronCommand($command);
        $status[0]->setToken($token = uniqid());
        $status[0]->setRanAt($now);
        $status[0]->setExitCode(0);

        $this->assertEquals($command, $status[0]->getCronCommand());
        $this->assertEquals($token, $status[0]->getToken());
        $this->assertEquals($now, $status[0]->getRanAt());
        $this->assertEquals(0, $status[0]->getExitCode());
        $this->assertNull($status[0]->getExitMessage());
        $this->assertNull($status[0]->getId());

        $status[1]->setCronCommand($command);
        $status[1]->setToken($token = uniqid());
        $status[1]->setExitCode(-1);
        $status[1]->setExitMessage('Call to member function getInformation on null');
        $status[1]->preUpdate();

        $this->assertEquals($command, $status[1]->getCronCommand());
        $this->assertEquals($token, $status[1]->getToken());
        $this->assertEquals(-1, $status[1]->getExitCode());
        $this->assertEquals('Call to member function getInformation on null', $status[1]->getExitMessage());
        $this->assertNull($status[1]->getId());
        $this->assertNotNull($status[1]->getRanAt());

        $command->addStatus($status[0]);
        $this->assertCount(1, $command->getStatus());

        $command->addStatus($status[1]);
        $this->assertCount(2, $command->getStatus());

        $command->removeStatus($status[0]);
        $this->assertCount(1, $command->getStatus());
        $this->assertContains($status[1], $command->getStatus());

        $manager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $manager->expects($this->once())->method('remove')->with($status[1]);

        /** @var EntityManager $manager */

        $command->clearStatus($manager);
        $this->assertCount(0, $command->getStatus());
    }
}
