<?php

namespace Nisanov\CronCommandBundle\Tests;

use Nisanov\CronCommandBundle\Command\CronCommandSetCommand;
use Nisanov\CronCommandBundle\Entity\CronCommand;
use Nisanov\CronCommandBundle\Service\CronCommandManager;
use Nisanov\CronCommandBundle\Tests\TestCase\CommandTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class CronCommandSetCommandTest.
 *
 * @package Nisanov\CronCommandBundle\Tests
 */
class CronCommandSetCommandTest extends CommandTestCase
{
    /**
     * Tests that the cron command set command requires interaction as expected.
     *
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage This command requires interaction.
     */
    public function testCronCommandSetCommandRequiresInteraction()
    {
        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:set');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName(), '--no-interaction' => null]);
    }

    /**
     * Tests that the cron command set command requires name as expected.
     *
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Command Failed: The command name is required.
     */
    public function testCronCommandSetCommandRequiresName()
    {
        // @formatter:off
        $question = $this->getMockBuilder(QuestionHelper::class)->disableOriginalConstructor()->getMock();
        $question
            ->expects($this->exactly(1))
            ->method('ask')
            ->willReturnOnConsecutiveCalls('');
        // @formatter:on

        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:set');

        /** @var QuestionHelper $question */
        $command->getHelperSet()->set($question, 'question');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName()]);

    }

    /**
     * Tests that the cron command set command is cancelled as expected.
     */
    public function testCronCommandSetCommandCancelled()
    {
        // @formatter:off
        $question = $this->getMockBuilder(QuestionHelper::class)->disableOriginalConstructor()->getMock();
        $question
            ->expects($this->exactly(2))
            ->method('ask')
            ->willReturnOnConsecutiveCalls('cron:command:test', 'Cancel');
        // @formatter:on

        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:set');

        /** @var QuestionHelper $question */
        $command->getHelperSet()->set($question, 'question');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName()]);

        $this->assertContains('The cron command request cancelled.', $tester->getDisplay());
    }

    /**
     * Tests that the cron command set command performs as expected.
     */
    public function testCronCommandSetCommandAsynchronousActive()
    {
        // @formatter:off
        $question = $this->getMockBuilder(QuestionHelper::class)->disableOriginalConstructor()->getMock();
        $question
            ->expects($this->exactly(6))
            ->method('ask')
            ->willReturnOnConsecutiveCalls('cron:command:test', 'Create New', '> /dev/null 2>&1', '*/5 * * * *', 'No', 'No');
        // @formatter:on

        $arguments = [self::$kernel->getContainer()->get('doctrine'), self::$kernel->getRootDir()];
        $manager = $this->getMockBuilder(CronCommandManager::class)->setConstructorArgs($arguments)->setMethods(['synchronize'])->getMock();
        $manager->expects($this->once())->method('synchronize');

        self::$kernel->getContainer()->set('nisanov_cron_command.manager', $manager);

        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:set');

        /** @var QuestionHelper $question */
        $command->getHelperSet()->set($question, 'question');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName()]);

        $output = $tester->getDisplay();

        $this->assertContains('*/5 * * * *', $output);
        $this->assertContains('cron:command:test > /dev/null 2>&1 [Asynchronous][Active]', $output);

        $command = self::$manager->getRepository('NisanovCronCommandBundle:CronCommand')->findOneBy(['name' => 'cron:command:test']);

        $this->assertInstanceOf(CronCommand::class, $command);
        $this->assertEquals('> /dev/null 2>&1', $command->getParameters());
        $this->assertEquals('*/5 * * * *', $command->getSchedule());
        $this->assertFalse($command->isLockable());
        $this->assertFalse($command->isDisabled());
    }

    /**
     * Tests that the cron command set command performs as expected.
     */
    public function testCronCommandSetCommandAsynchronousDisabled()
    {
        // @formatter:off
        $question = $this->getMockBuilder(QuestionHelper::class)->disableOriginalConstructor()->getMock();
        $question
            ->expects($this->exactly(6))
            ->method('ask')
            ->willReturnOnConsecutiveCalls('cron:command:test', 'Create New', '> /dev/null 2>&1', '*/5 * * * *', 'No', 'Yes');
        // @formatter:on

        $arguments = [self::$kernel->getContainer()->get('doctrine'), self::$kernel->getRootDir()];
        $manager = $this->getMockBuilder(CronCommandManager::class)->setConstructorArgs($arguments)->setMethods(['synchronize'])->getMock();
        $manager->expects($this->once())->method('synchronize');

        self::$kernel->getContainer()->set('nisanov_cron_command.manager', $manager);

        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:set');

        /** @var QuestionHelper $question */
        $command->getHelperSet()->set($question, 'question');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName()]);

        $output = $tester->getDisplay();

        $this->assertContains('*/5 * * * *', $output);
        $this->assertContains('cron:command:test > /dev/null 2>&1 [Asynchronous][Disabled]', $output);

        $command = self::$manager->getRepository('NisanovCronCommandBundle:CronCommand')->findOneBy(['name' => 'cron:command:test']);

        $this->assertInstanceOf(CronCommand::class, $command);
        $this->assertEquals('> /dev/null 2>&1', $command->getParameters());
        $this->assertEquals('*/5 * * * *', $command->getSchedule());
        $this->assertFalse($command->isLockable());
        $this->assertTrue($command->isDisabled());
    }

    /**
     * Tests that the cron command set command performs as expected.
     */
    public function testCronCommandSetCommandLockableActive()
    {
        // @formatter:off
        $question = $this->getMockBuilder(QuestionHelper::class)->disableOriginalConstructor()->getMock();
        $question
            ->expects($this->exactly(6))
            ->method('ask')
            ->willReturnOnConsecutiveCalls('cron:command:test', 'Create New', '> /dev/null 2>&1', '*/5 * * * *', 'Yes', 'No');
        // @formatter:on

        $arguments = [self::$kernel->getContainer()->get('doctrine'), self::$kernel->getRootDir()];
        $manager = $this->getMockBuilder(CronCommandManager::class)->setConstructorArgs($arguments)->setMethods(['synchronize'])->getMock();
        $manager->expects($this->once())->method('synchronize');

        self::$kernel->getContainer()->set('nisanov_cron_command.manager', $manager);

        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:set');

        /** @var QuestionHelper $question */
        $command->getHelperSet()->set($question, 'question');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName()]);

        $output = $tester->getDisplay();

        $this->assertContains('*/5 * * * *', $output);
        $this->assertContains('cron:command:test > /dev/null 2>&1 [Lockable][Active]', $output);

        $command = self::$manager->getRepository('NisanovCronCommandBundle:CronCommand')->findOneBy(['name' => 'cron:command:test']);

        $this->assertInstanceOf(CronCommand::class, $command);
        $this->assertEquals('> /dev/null 2>&1', $command->getParameters());
        $this->assertEquals('*/5 * * * *', $command->getSchedule());
        $this->assertTrue($command->isLockable());
        $this->assertFalse($command->isDisabled());
    }

    /**
     * Tests that the cron command set command performs as expected.
     */
    public function testCronCommandSetCommandLockableDisabled()
    {
        // @formatter:off
        $question = $this->getMockBuilder(QuestionHelper::class)->disableOriginalConstructor()->getMock();
        $question
            ->expects($this->exactly(6))
            ->method('ask')
            ->willReturnOnConsecutiveCalls('cron:command:test', 'Create New', '> /dev/null 2>&1', '*/5 * * * *', 'Yes', 'Yes');
        // @formatter:on

        $arguments = [self::$kernel->getContainer()->get('doctrine'), self::$kernel->getRootDir()];
        $manager = $this->getMockBuilder(CronCommandManager::class)->setConstructorArgs($arguments)->setMethods(['synchronize'])->getMock();
        $manager->expects($this->once())->method('synchronize');

        self::$kernel->getContainer()->set('nisanov_cron_command.manager', $manager);

        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:set');

        /** @var QuestionHelper $question */
        $command->getHelperSet()->set($question, 'question');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName()]);

        $output = $tester->getDisplay();

        $this->assertContains('*/5 * * * *', $output);
        $this->assertContains('cron:command:test > /dev/null 2>&1 [Lockable][Disabled]', $output);

        $command = self::$manager->getRepository('NisanovCronCommandBundle:CronCommand')->findOneBy(['name' => 'cron:command:test']);

        $this->assertInstanceOf(CronCommand::class, $command);
        $this->assertEquals('> /dev/null 2>&1', $command->getParameters());
        $this->assertEquals('*/5 * * * *', $command->getSchedule());
        $this->assertTrue($command->isLockable());
        $this->assertTrue($command->isDisabled());
    }

    /**
     * Tests that the cron command set command requires a matching command to remove.
     *
     * @expectedException \UnexpectedValueException
     * @expectedExceptionMessage No commands were found matching the selected command name, please restart and try again.
     */
    public function testCronCommandSetCommandRemovedNotFound()
    {
        $command = new CronCommand();
        $command->setName('cron:command:test');
        $command->setParameters('> /dev/null 2>&1');
        $command->setSchedule('@weekly');
        $command->setLockable(true);
        $command->setDisabled(true);

        self::$manager->persist($command);
        self::$manager->flush();

        // @formatter:off
        $question = $this->getMockBuilder(QuestionHelper::class)->disableOriginalConstructor()->getMock();
        $question
            ->expects($this->exactly(1))
            ->method('ask')
            ->willReturnOnConsecutiveCalls('cron:command:wrong');
        // @formatter:on

        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:set');

        /** @var QuestionHelper $question */
        $command->getHelperSet()->set($question, 'question');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName(), '--remove' => null]);
    }

    /**
     * Tests that the cron command set command is modified as expected.
     */
    public function testCronCommandSetCommandModified()
    {
        $command = new CronCommand();
        $command->setName('cron:command:test');
        $command->setParameters('> /dev/null 2>&1');
        $command->setSchedule('@weekly');
        $command->setLockable(true);
        $command->setDisabled(true);

        self::$manager->persist($command);
        self::$manager->flush();

        // @formatter:off
        $question = $this->getMockBuilder(QuestionHelper::class)->disableOriginalConstructor()->getMock();
        $question
            ->expects($this->exactly(6))
            ->method('ask')
            ->willReturnOnConsecutiveCalls('cron:command:test', 'cron:command:test', '> test.log', '@monthly', 'No', 'No');
        // @formatter:on

        $arguments = [self::$kernel->getContainer()->get('doctrine'), self::$kernel->getRootDir()];
        $manager = $this->getMockBuilder(CronCommandManager::class)->setConstructorArgs($arguments)->setMethods(['synchronize'])->getMock();
        $manager->expects($this->once())->method('synchronize');

        self::$kernel->getContainer()->set('nisanov_cron_command.manager', $manager);

        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:set');

        /** @var QuestionHelper $question */
        $command->getHelperSet()->set($question, 'question');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName()]);

        $output = $tester->getDisplay();

        $this->assertContains('@monthly', $tester->getDisplay());
        $this->assertContains('cron:command:test > test.log [Asynchronous][Active]', $output);

        $command = self::$manager->getRepository('NisanovCronCommandBundle:CronCommand')->findOneBy(['name' => 'cron:command:test']);

        $this->assertInstanceOf(CronCommand::class, $command);
        $this->assertEquals('> test.log', $command->getParameters());
        $this->assertEquals('@monthly', $command->getSchedule());
        $this->assertFalse($command->isLockable());
        $this->assertFalse($command->isDisabled());
    }

    /**
     * Tests that the cron command set command removed as expected.
     */
    public function testCronCommandSetCommandRemoved()
    {
        $command = new CronCommand();
        $command->setName('cron:command:test');
        $command->setParameters('> /dev/null 2>&1');
        $command->setSchedule('@yearly');
        $command->setLockable(true);
        $command->setDisabled(true);

        self::$manager->persist($command);
        self::$manager->flush();

        $command = self::$manager->getRepository('NisanovCronCommandBundle:CronCommand')->findOneBy(['name' => 'cron:command:test']);

        $this->assertInstanceOf(CronCommand::class, $command);

        // @formatter:off
        $question = $this->getMockBuilder(QuestionHelper::class)->disableOriginalConstructor()->getMock();
        $question
            ->expects($this->exactly(2))
            ->method('ask')
            ->willReturnOnConsecutiveCalls('cron:command:test', 'cron:command:test');
        // @formatter:on

        $arguments = [self::$kernel->getContainer()->get('doctrine'), self::$kernel->getRootDir()];
        $manager = $this->getMockBuilder(CronCommandManager::class)->setConstructorArgs($arguments)->setMethods(['synchronize'])->getMock();
        $manager->expects($this->once())->method('synchronize');

        self::$kernel->getContainer()->set('nisanov_cron_command.manager', $manager);

        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:set');

        /** @var QuestionHelper $question */
        $command->getHelperSet()->set($question, 'question');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName(), '--remove' => null]);

        $this->assertContains('The selected cron command has been removed.', $tester->getDisplay());

        $command = self::$manager->getRepository('NisanovCronCommandBundle:CronCommand')->findOneBy(['name' => 'cron:command:test']);

        $this->assertNull($command);
    }

    /**
     * Tests that the cron command set command removed requires name as expected.
     *
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Command Failed: The command name is required.
     */
    public function testCronCommandSetCommandRemovedRequiresName()
    {
        // @formatter:off
        $question = $this->getMockBuilder(QuestionHelper::class)->disableOriginalConstructor()->getMock();
        $question
            ->expects($this->exactly(1))
            ->method('ask')
            ->willReturnOnConsecutiveCalls('');
        // @formatter:on

        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:set');

        /** @var QuestionHelper $question */
        $command->getHelperSet()->set($question, 'question');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName(), '--remove' => null]);
    }

    /**
     * Tests that the cron command set command requires the parameter parameter as expected.
     *
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Command Failed: Missing the cron command parameter option.
     */
    public function testCronCommandSetCommandMissingParameterParameter()
    {
        // @formatter:off
        $question = $this->getMockBuilder(QuestionHelper::class)->disableOriginalConstructor()->getMock();
        $question
            ->expects($this->exactly(3))
            ->method('ask')
            ->willReturnOnConsecutiveCalls('cron:command:test', 'Create New', '');
        // @formatter:on

        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:set');

        /** @var QuestionHelper $question */
        $command->getHelperSet()->set($question, 'question');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName()]);
    }

    /**
     * Tests that the cron command set command requires the schedule parameter as expected.
     *
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Command Failed: Missing the cron command schedule option.
     */
    public function testCronCommandSetCommandMissingScheduleParameter()
    {
        // @formatter:off
        $question = $this->getMockBuilder(QuestionHelper::class)->disableOriginalConstructor()->getMock();
        $question
            ->expects($this->exactly(4))
            ->method('ask')
            ->willReturnOnConsecutiveCalls('cron:command:test', 'Create New', '> /dev/null 2>&1', '');
        // @formatter:on

        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:set');

        /** @var QuestionHelper $question */
        $command->getHelperSet()->set($question, 'question');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName()]);
    }
}
