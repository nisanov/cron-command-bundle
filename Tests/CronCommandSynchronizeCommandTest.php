<?php

namespace Nisanov\CronCommandBundle\Tests;

use Nisanov\CronCommandBundle\Command\CronCommandSetCommand;
use Nisanov\CronCommandBundle\Service\CronCommandManager;
use Nisanov\CronCommandBundle\Tests\TestCase\CommandTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class CronCommandSynchronizeCommandTest.
 *
 * @package Nisanov\CronCommandBundle\Tests
 */
class CronCommandSynchronizeCommandTest extends CommandTestCase
{
    /**
     * Tests that the cron command synchronize command performs as expected.
     */
    public function testCronCommandSynchronize()
    {
        $arguments = [self::$kernel->getContainer()->get('doctrine'), self::$kernel->getRootDir()];
        $manager = $this->getMockBuilder(CronCommandManager::class)->setConstructorArgs($arguments)->setMethods(['synchronize'])->getMock();
        $manager->expects($this->once())->method('synchronize');

        self::$kernel->getContainer()->set('nisanov_cron_command.manager', $manager);

        $application = new Application(self::$kernel);
        $application->add(new CronCommandSetCommand());

        $command = $application->find('cron:command:synchronize');

        $tester = new CommandTester($command);
        $tester->execute(['command' => $command->getName()]);

        $output = $tester->getDisplay();

        $this->assertContains('The commands have been synchronized with the crontab.', $output);
    }
}
