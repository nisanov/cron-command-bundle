<?php

namespace Nisanov\CronCommandBundle\Tests;

use Nisanov\CronCommandBundle\Entity\CronCommand;
use Nisanov\CronCommandBundle\Entity\CronCommandStatus;
use Nisanov\CronCommandBundle\EventListener\CommandListener;
use Nisanov\CronCommandBundle\Tests\TestCase\CommandTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Event\ConsoleExceptionEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class CommandListenerTest.
 *
 * @package Nisanov\CronCommandBundle\Tests
 */
class CommandListenerTest extends CommandTestCase
{
    /**
     * Tests that console command event command is whitelisted as expected.
     */
    public function testConsoleCommandCommandWhitelisted()
    {
        $command = new Command('cache:clear');

        $event = $this->getMockBuilder(ConsoleCommandEvent::class)->disableOriginalConstructor()->getMock();
        $event->expects($this->once())->method('getCommand')->willReturn($command);

        /** @var ConsoleCommandEvent $event */

        $listener = $this->getMockBuilder(CommandListener::class)->setConstructorArgs([self::$kernel->getContainer()->get('doctrine')])->setMethods(['getCommand'])->getMock();
        $listener->expects($this->never())->method('getCommand');

        /** @var CommandListener $listener */

        $listener->consoleCommand($event);
    }

    /**
     * Tests that console command event command is not found as expected.
     */
    public function testConsoleCommandCommandNotFound()
    {
        $command = new Command('cron:command:test');

        $event = $this->getMockBuilder(ConsoleCommandEvent::class)->disableOriginalConstructor()->getMock();
        $event->expects($this->once())->method('getCommand')->willReturn($command);
        $event->expects($this->never())->method('getOutput');

        /** @var ConsoleCommandEvent $event */

        $listener = new CommandListener(self::$kernel->getContainer()->get('doctrine'));
        $listener->consoleCommand($event);
    }

    /**
     * Tests that console command event command is disabled as expected.
     */
    public function testConsoleCommandCommandIsDisabled()
    {
        $cron = new CronCommand();
        $cron->setName('cron:command:test');
        $cron->setParameters('> /dev/null 2>&1');
        $cron->setSchedule('@hourly');
        $cron->setDisabled(true);

        self::$manager->persist($cron);
        self::$manager->flush();

        $command = new Command('cron:command:test');

        $output = $this->getMockBuilder(ConsoleOutput::class)->disableOriginalConstructor()->getMock();
        $output->expects($this->once())->method('writeln')->with('<error>The command [cron:command:test] is disabled.</error>');

        $event = $this->getMockBuilder(ConsoleCommandEvent::class)->disableOriginalConstructor()->getMock();
        $event->expects($this->once())->method('getCommand')->willReturn($command);
        $event->expects($this->once())->method('getOutput')->willReturn($output);

        /** @var ConsoleCommandEvent $event */

        $listener = new CommandListener(self::$kernel->getContainer()->get('doctrine'));
        $listener->consoleCommand($event);

        self::$manager->refresh($cron);

        /** @var CronCommandStatus[] $status */

        $this->assertCount(1, $status = $cron->getStatus());
        $this->assertInstanceOf(CronCommandStatus::class, $status[0]);
    }

    /**
     * Tests that console command event command is locked as expected.
     */
    public function testConsoleCommandCommandIsLocked()
    {
        $cron = new CronCommand();
        $cron->setName('cron:command:test');
        $cron->setParameters('> /dev/null 2>&1');
        $cron->setSchedule('@hourly');
        $cron->setDisabled(false);
        $cron->setLockable(true);
        $cron->setLocked(true);

        self::$manager->persist($cron);
        self::$manager->flush();

        $command = new Command('cron:command:test');

        $output = $this->getMockBuilder(ConsoleOutput::class)->disableOriginalConstructor()->getMock();
        $output->expects($this->once())->method('writeln')->with('<error>The command [cron:command:test] is locked by another process.</error>');

        $event = $this->getMockBuilder(ConsoleCommandEvent::class)->disableOriginalConstructor()->getMock();
        $event->expects($this->once())->method('getCommand')->willReturn($command);
        $event->expects($this->once())->method('getOutput')->willReturn($output);

        /** @var ConsoleCommandEvent $event */

        $listener = new CommandListener(self::$kernel->getContainer()->get('doctrine'));
        $listener->consoleCommand($event);

        self::$manager->refresh($cron);

        /** @var CronCommandStatus[] $status */

        $this->assertCount(1, $status = $cron->getStatus());
        $this->assertInstanceOf(CronCommandStatus::class, $status[0]);
    }

    /**
     * Tests that console command event command gets locked as expected.
     */
    public function testConsoleCommandCommandGetsLocked()
    {
        $cron = new CronCommand();
        $cron->setName('cron:command:test');
        $cron->setParameters('> /dev/null 2>&1');
        $cron->setSchedule('@hourly');
        $cron->setDisabled(false);
        $cron->setLockable(true);
        $cron->setLocked(false);

        self::$manager->persist($cron);
        self::$manager->flush();

        $command = new Command('cron:command:test');

        $output = $this->getMockBuilder(ConsoleOutput::class)->disableOriginalConstructor()->getMock();
        $output->expects($this->never())->method('writeln');

        $event = $this->getMockBuilder(ConsoleCommandEvent::class)->disableOriginalConstructor()->getMock();
        $event->expects($this->once())->method('getCommand')->willReturn($command);
        $event->expects($this->once())->method('getOutput')->willReturn($output);

        /** @var ConsoleCommandEvent $event */

        $listener = new CommandListener(self::$kernel->getContainer()->get('doctrine'));
        $listener->consoleCommand($event);

        self::$manager->refresh($cron);

        /** @var CronCommandStatus[] $status */

        $this->assertCount(1, $status = $cron->getStatus());
        $this->assertInstanceOf(CronCommandStatus::class, $status[0]);
        $this->assertTrue($cron->isLocked());
    }

    /**
     * Tests that console exception event command is whitelisted as expected.
     */
    public function testConsoleExceptionCommandWhitelisted()
    {
        $command = new Command('cache:clear');

        $event = $this->getMockBuilder(ConsoleExceptionEvent::class)->disableOriginalConstructor()->getMock();
        $event->expects($this->once())->method('getCommand')->willReturn($command);

        /** @var ConsoleExceptionEvent $event */

        $listener = $this->getMockBuilder(CommandListener::class)->setConstructorArgs([self::$kernel->getContainer()->get('doctrine')])->setMethods(['getCommand'])->getMock();
        $listener->expects($this->never())->method('getCommand');

        /** @var CommandListener $listener */

        $listener->consoleException($event);
    }

    /**
     * Tests that console exception event command is not found as expected.
     */
    public function testConsoleExceptionCommandNotFound()
    {
        $command = new Command('cron:command:test');

        $event = $this->getMockBuilder(ConsoleExceptionEvent::class)->disableOriginalConstructor()->getMock();
        $event->expects($this->once())->method('getCommand')->willReturn($command);
        $event->expects($this->never())->method('getException');

        /** @var ConsoleExceptionEvent $event */

        $listener = new CommandListener(self::$kernel->getContainer()->get('doctrine'));
        $listener->consoleException($event);
    }

    /**
     * Tests that console exception event command performs as expected.
     */
    public function testConsoleException()
    {
        $cron = new CronCommand();
        $cron->setName('cron:command:test');
        $cron->setParameters('> /dev/null 2>&1');
        $cron->setSchedule('@hourly');
        $cron->setDisabled(false);
        $cron->setLockable(true);
        $cron->setLocked(true);

        self::$manager->persist($cron);
        self::$manager->flush();

        $command = new Command('cron:command:test');

        $exception = new \Exception('Exception Test', -1);

        $event = $this->getMockBuilder(ConsoleExceptionEvent::class)->disableOriginalConstructor()->getMock();
        $event->expects($this->once())->method('getCommand')->willReturn($command);
        $event->expects($this->once())->method('getException')->willReturn($exception);
        $event->expects($this->once())->method('getExitCode')->willReturn($exception->getCode());

        /** @var ConsoleExceptionEvent $event */

        $listener = new CommandListener(self::$kernel->getContainer()->get('doctrine'));
        $listener->consoleException($event);

        self::$manager->refresh($cron);

        /** @var CronCommandStatus[] $status */

        $this->assertCount(1, $status = $cron->getStatus());
        $this->assertInstanceOf(CronCommandStatus::class, $status[0]);
        $this->assertEquals(-1, $status[0]->getExitCode());
        $this->assertEquals('Exception Test', $status[0]->getExitMessage());
        $this->assertFalse($cron->isLocked());
    }

    /**
     * Tests that console terminated event command is whitelisted as expected.
     */
    public function testConsoleTerminatedCommandWhitelisted()
    {
        $command = new Command('cache:clear');

        $event = $this->getMockBuilder(ConsoleTerminateEvent::class)->disableOriginalConstructor()->getMock();
        $event->expects($this->once())->method('getCommand')->willReturn($command);

        /** @var ConsoleTerminateEvent $event */

        $listener = $this->getMockBuilder(CommandListener::class)->setConstructorArgs([self::$kernel->getContainer()->get('doctrine')])->setMethods(['getCommand'])->getMock();
        $listener->expects($this->never())->method('getCommand');

        /** @var CommandListener $listener */

        $listener->consoleTerminated($event);
    }

    /**
     * Tests that console terminated event command is not found as expected.
     */
    public function testConsoleTerminatedCommandNotFound()
    {
        $command = new Command('cron:command:test');

        $event = $this->getMockBuilder(ConsoleTerminateEvent::class)->disableOriginalConstructor()->getMock();
        $event->expects($this->once())->method('getCommand')->willReturn($command);
        $event->expects($this->never())->method('getOutput');

        /** @var ConsoleTerminateEvent $event */

        $listener = new CommandListener(self::$kernel->getContainer()->get('doctrine'));
        $listener->consoleTerminated($event);
    }

    /**
     * Tests that console exception event command set exit code locked as expected.
     */
    public function testConsoleTerminatedSetExitCodeLocked()
    {
        $cron = new CronCommand();
        $cron->setName('cron:command:test');
        $cron->setParameters('> /dev/null 2>&1');
        $cron->setSchedule('@hourly');
        $cron->setDisabled(false);
        $cron->setLockable(true);
        $cron->setLocked(true);

        self::$manager->persist($cron);
        self::$manager->flush();

        $command = new Command('cron:command:test');

        $event = $this->getMockBuilder(ConsoleTerminateEvent::class)->disableOriginalConstructor()->getMock();
        $event->expects($this->once())->method('getCommand')->willReturn($command);
        $event->expects($this->once())->method('getExitCode')->willReturn(CronCommandStatus::RETURN_CODE_DISABLED);

        /** @var ConsoleTerminateEvent $event */

        $listener = new CommandListener(self::$kernel->getContainer()->get('doctrine'));
        $listener->consoleTerminated($event);

        self::$manager->refresh($cron);

        /** @var CronCommandStatus[] $status */

        $this->assertCount(1, $status = $cron->getStatus());
        $this->assertInstanceOf(CronCommandStatus::class, $status[0]);
        $this->assertEquals(CronCommandStatus::RETURN_CODE_LOCKED, $status[0]->getExitCode());
        $this->assertFalse($cron->isLocked());
    }

    /**
     * Tests that console exception event command set exit code successful as expected.
     */
    public function testConsoleTerminatedSetExitCodeSuccessful()
    {
        $cron = new CronCommand();
        $cron->setName('cron:command:test');
        $cron->setParameters('> /dev/null 2>&1');
        $cron->setSchedule('@hourly');
        $cron->setDisabled(false);
        $cron->setLockable(true);
        $cron->setLocked(true);

        self::$manager->persist($cron);
        self::$manager->flush();

        $command = new Command('cron:command:test');

        $event = $this->getMockBuilder(ConsoleTerminateEvent::class)->disableOriginalConstructor()->getMock();
        $event->expects($this->once())->method('getCommand')->willReturn($command);
        $event->expects($this->exactly(2))->method('getExitCode')->willReturn(CronCommandStatus::RETURN_CODE_SUCCESSFUL, CronCommandStatus::RETURN_CODE_SUCCESSFUL);

        /** @var ConsoleTerminateEvent $event */

        $listener = new CommandListener(self::$kernel->getContainer()->get('doctrine'));
        $listener->consoleTerminated($event);

        self::$manager->refresh($cron);

        /** @var CronCommandStatus[] $status */

        $this->assertCount(1, $status = $cron->getStatus());
        $this->assertInstanceOf(CronCommandStatus::class, $status[0]);
        $this->assertEquals(CronCommandStatus::RETURN_CODE_SUCCESSFUL, $status[0]->getExitCode());
        $this->assertFalse($cron->isLocked());
    }
}
