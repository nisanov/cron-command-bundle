<?php

namespace Nisanov\CronCommandBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class NisanovCronCommandBundle.
 *
 * @package Nisanov\CronCommandBundle
 */
class NisanovCronCommandBundle extends Bundle
{
}
