# Change Log

## [1.0.4](https://gitlab.com/nisanov/cron-command-bundle/tree/1.0.4) (30-Aug-2017)

#### [Full Change Log](https://gitlab.com/nisanov/cron-command-bundle/compare/1.0.3...1.0.4)

**Bugs:**

* Resolve: update method used to derive symfony console path.

## [1.0.3](https://gitlab.com/nisanov/cron-command-bundle/tree/1.0.3) (01-Jun-2017)

#### [Full Change Log](https://gitlab.com/nisanov/cron-command-bundle/compare/1.0.2...1.0.3)

**Enhancements:**

* Resolve [#9](https://gitlab.com/nisanov/cron-command-bundle/issues/9): Add synchronize command.

**Tests:**

* Resolve [#10](https://gitlab.com/nisanov/cron-command-bundle/issues/10): Complete code coverage.

## [1.0.2](https://gitlab.com/nisanov/cron-command-bundle/tree/1.0.2) (28-May-2017)

#### [Full Change Log](https://gitlab.com/nisanov/cron-command-bundle/compare/1.0.1...1.0.2)

**Enhancements:**

* Resolve [#6](https://gitlab.com/nisanov/cron-command-bundle/issues/6): Add Created At, Exit Code, and Exit Exception statistic.

**Bugs:**

* Resolve [#1](https://gitlab.com/nisanov/cron-command-bundle/issues/1): Schedule modification results in an isOverdue failure.

**Tests:**

* Resolve [#7](https://gitlab.com/nisanov/cron-command-bundle/issues/7): Unit test cron command set command.
* Resolve [#8](https://gitlab.com/nisanov/cron-command-bundle/issues/8): Unit test cron command statistics command.

## [1.0.1](https://gitlab.com/nisanov/cron-command-bundle/tree/1.0.1) (21-May-2017)

#### [Full Change Log](https://gitlab.com/nisanov/cron-command-bundle/compare/1.0.0...1.0.1)

**Bugs:**

* Fix issue with commands with no associated status.

**Enhancements:**

* Resolve [#2](https://gitlab.com/nisanov/cron-command-bundle/issues/2): Unit test cron manager service provider.
* Resolve [#3](https://gitlab.com/nisanov/cron-command-bundle/issues/3): Standardize namespace placement.
* Resolve [#4](https://gitlab.com/nisanov/cron-command-bundle/issues/4): Missing Doctrine update command requirement during installation.

**Codacy Recommendations:**

* Resolve [#5](https://gitlab.com/nisanov/cron-command-bundle/issues/5): Clear Codacy Issues.

## [1.0.0](https://gitlab.com/nisanov/cron-command-bundle/tree/1.0.0) (16-May-2017)
