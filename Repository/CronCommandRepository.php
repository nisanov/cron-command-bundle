<?php

namespace Nisanov\CronCommandBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class CronCommandRepository.
 *
 * @package Nisanov\CronCommandBundle\Repository
 */
class CronCommandRepository extends EntityRepository
{
    // ...
}
