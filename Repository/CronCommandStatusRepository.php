<?php

namespace Nisanov\CronCommandBundle\Repository;

use Nisanov\CronCommandBundle\Entity\CronCommand;
use Nisanov\CronCommandBundle\Entity\CronCommandStatus;
use Doctrine\ORM\EntityRepository;

/**
 * Class CronCommandStatusRepository.
 *
 * @package Nisanov\CronCommandBundle\Repository
 */
class CronCommandStatusRepository extends EntityRepository
{
    /**
     * Get the last successful cron command status for the specified command.
     *
     * @param CronCommand $command
     *
     * @return CronCommandStatus
     */
    public function findByScopeLastSuccessfulByCommand(CronCommand $command): ?CronCommandStatus
    {
        /** @var CronCommandStatus $status */
        $status = $this->findOneBy([
            'cronCommand' => $command,
            'exitCode' => CronCommandStatus::RETURN_CODE_SUCCESSFUL,
        ], ['ranAt' => 'DESC']);

        return $status;
    }

    /**
     * Get the last cron command status for the specified command.
     *
     * @param CronCommand $command
     *
     * @return CronCommandStatus
     */
    public function findByScopeLastByCommand(CronCommand $command): ?CronCommandStatus
    {
        /** @var CronCommandStatus $status */
        $status = $this->findOneBy([
            'cronCommand' => $command,
        ], ['ranAt' => 'DESC']);

        return $status;
    }
}
