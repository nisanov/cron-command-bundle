# Cron Command Bundle

A Symfony bundle that manages and integrates commands with the Linux crontab, by tracking command execution status and identifying commands that are overdue.

[![License](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](LICENSE)
[![Latest Stable Version](https://img.shields.io/packagist/v/nisanov/cron-command-bundle.svg?style=flat-square)](https://packagist.org/packages/nisanov/cron-command-bundle)
[![coverage report](https://gitlab.com/nisanov/cron-command-bundle/badges/master/coverage.svg)](https://gitlab.com/nisanov/cron-command-bundle/commits/master)
[![build status](https://gitlab.com/nisanov/cron-command-bundle/badges/master/build.svg)](https://gitlab.com/nisanov/cron-command-bundle/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/06eba134052244a6932b2fdfdb1b941d)](https://www.codacy.com/app/yasha.nisanov/cron-command-bundle?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=nisanov/cron-command-bundle&amp;utm_campaign=Badge_Grade)

1. [Installation](#installation)
2. [Usage](#usage)
2. [Testing](#testing)

## Installation

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download th
e latest stable version of this bundle:

```console
$ composer require nisanov/cron-command-bundle
```

This command requires you to have Composer installed globally, as explained
in the [installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            
            // ...

            new Nisanov\CronCommandBundle\NisanovCronCommandBundle(),
        ];

        // ...
    }

    // ...
}
```

### Step 3: Update the database

```bash
php bin/console doctrine:schema:update --force
```

## Usage

#### Adding/Modifying a command to the database and crontab

```bash
php bin/console cron:command:set
```

Follow the prompts after executing the above command to add or modify a command to the database and the crontab.

#### Removing a command from the database and crontab

```bash
php bin/console cron:command:set --remove
```

Follow the prompts after executing the above command to remove a command from the database and the crontab.

#### Synchronizing commands with the crontab after any manual manipulation of either.

```bash
php bin/console cron:command:synchronize
```

#### Getting statistical information on active commands stored in the database and crontab

```bash
php bin/console cron:command:statistics
```

Statistical information is displayed in the console after executing the above command.  

The following service request provides the same information that can be utilized by other aspects of your application.

```php
// reference the container in a context applicable way
$container->get('nisanov_cron_command.manager')->statistics();
```

##### Example

```bash
 ======================== =========== ===================== ===================== ===================== ============ =========== ==============
  Name                     Schedule    Created At            Last Success Ran At   Next Run At           Is Overdue   Exit Code   Exit Message                                   
 ======================== =========== ===================== ===================== ===================== ============ =========== ==============
  swiftmailer:spool:send   * * * * *   2017-05-15 10:15:30   2017-05-15 10:16:02   2017-05-15 10:17:00   0            0                                                          
 ======================== =========== ===================== ===================== ===================== ============ =========== ==============
```

## Testing

### Run phpunit tests with the following command:

```bash
vendor/bin/phpunit -c vendor/nisanov/cron-command-bundle/phpunit.xml.dist
```

> WARNING: The unit tests manipulate the real crontab.